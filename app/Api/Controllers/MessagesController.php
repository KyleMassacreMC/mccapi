<?php

namespace Api\Controllers;

use App\User;
use App\Messages;
use Dingo\Api\Facade\API;
use Illuminate\Http\Request;
use Api\Requests\MailRequest;
use Tymon\JWTAuth\Facades\JWTAuth;
use Api\Transformers\MailTransformer;
use League\Fractal\Pagination\Cursor;
use League\Fractal\Resource\Collection;
use Tymon\JWTAuth\Exceptions\JWTException;
use League\Fractal\Pagination\IlluminatePaginatorAdapter;

/**
 * Class MessagesController
 *
 * @package Api\Controllers
 */
class MessagesController extends AuthenticatedController
{

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\request $request
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $currentCursor = $request->get('cursor', null);
        $previousCursor = $request->get('previous', null);
        $limit = $request->get('limit', 10);

        if ($currentCursor)
        {
            $mail = Messages::where(function ($query) use ($currentCursor)
            {
                $query->where('mail_to', $this->user->userid)
                    ->where('mail_id', '>', $currentCursor)
                    ->orWhere('mail_from', $this->user->userid);
            })
                ->paginate($limit);
        }
        else
        {
            $mail = Messages::where(function ($query)
            {
                $query->where('mail_to', $this->user->userid)
                    ->orWhere('mail_from', $this->user->userid);
            })
                ->paginate($limit);
        }
        return $this->response->paginator($mail, new MailTransformer);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Api\Requests\MailRequest $request
     *
     * @return \Illuminate\Http\Response
     */
    public function store(MailRequest $request)
    {
        $mail = new Messages;
        $mail->mail_to = $request->input('mail_to');
        $mail->mail_from = $this->user->userid;
        $mail->mail_read = 0;
        $mail->mail_time = time();
        $mail->mail_subject = $request->input('mail_subject');
        $mail->mail_text = $request->get('mail_text');
        $mail->save();

        $this->user->incrementNewMail();

        return $this->response->item($mail, new MailTransformer());
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $message = Messages::find($id);
        if ($message != null)
        {
            if (!in_array($this->user->userid, [$message->mail_to, $message->mail_from]))
            {
                return $this->response->errorUnauthorized('This message does not belong to you');
            }
            else
            {
                if ($message->mail_read == 0)
                {
                    $this->user->decrementNewMail();
                    $message->mail_read = 1;
                    $message->save();
                }
                return $this->response->item($message, new MailTransformer);
            }
        }
        else
        {
            return $this->response->errorNotFound('Message not found');
        }


    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $messages = Messages::find($id);
        if ($messages->mail_to != $this->user->userid)
        {
            return $this->response->errorUnauthorized('This message does not belong to you');
        }
        else
        {
            if($messages->mail_read == 0)
            {
                $this->user->decrementNewMail();
            }
            $messages->destroy([$id]);
            return $this->response->array(['message' => 'Deleted']);
        }
    }
}
