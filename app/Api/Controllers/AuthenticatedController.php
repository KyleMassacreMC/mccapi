<?php
/**
 * Created by PhpStorm.
 * User: kyle
 * Date: 3/21/16
 * Time: 4:16 AM
 */

namespace Api\Controllers;

use Tymon\JWTAuth\Facades\JWTAuth;


class AuthenticatedController extends BaseController
{
    public $user;
    public function __construct()
    {
        $this->user = JWTAuth::user();

        if($this->user)
        {
            $this->user->check_level();
        }
    }
}