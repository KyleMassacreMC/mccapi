<?php
/**
 * Created by PhpStorm.
 * User: kyle
 * Date: 3/23/16
 * Time: 6:25 AM
 */

namespace Api\Controllers;

use Api\Transformers\UserTransformer;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;


class UsersController extends AuthenticatedController
{
    public function me(Request $request)
    {
        return $this->response->item($this->user, new UserTransformer);
    }

    public function validateToken()
    {
        // Our routes file should have already authenticated this token, so we just return success here
        return $this->response->array(['status' => 'success'])->statusCode(200);
    }

    public function logout()
    {
        JWTAuth::invalidate();
    }

}