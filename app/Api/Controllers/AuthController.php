<?php

namespace Api\Controllers;

use App\User;
use App\UserStat;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Facades\JWTAuth;
use Api\Requests\RegistrationRequest;
use Tymon\JWTAuth\Exceptions\JWTException;

class AuthController extends BaseController
{

    public function authenticate(Request $request)
    {
        $find = User::where('login_name', $request->get('username'))->first();
        if($find != NULL)
        {
            try {
                if(verify_user_password($request->get('password'),$find->pass_salt,$find->userpass) )
                {
                    if($token = JWTAuth::fromUser($find))
                    {
                        $find->lastip_login = $request->ip();
                        $find->lastip = $request->ip();
                        $find->last_login = time();
                        $find->laston = time();
                        $find->save();

                        return $this->response->array(['auth' => false,'token' => $token]);
                    }
                    else
                    {
                        return $this->response->array(['auth' => false, 'error' => 'invalid_credentials'], 401);
                    }
                }
                else
                {
                    return $this->response->array(['auth' => false, 'error' => 'invalid_credentials'], 401);
                }
            }
            catch (JWTException $e){
                return $this->response->array(['error' => $e->getMessage()], 500);
            }

        }
        else
        {
            return $this->response->array(['auth' => false, 'error' => 'invalid_credentials'], 401);
        }
    }

    public function register(RegistrationRequest $request)
    {
        $salt = generate_pass_salt();
        $user = new User;
        $user->level = 1;
        $user->username = $request->get('username');
        $user->login_name = $request->get('username');
        $user->email = $request->get('email');
        $user->userpass = encode_password($request->get('password'),$salt);
        $user->pass_salt = $salt;
        $user->gender = $request->get('gender');
        $user->lastip_signup = $request->ip();
        $user->lastip_login = $request->ip();
        $user->lastip = $request->ip();
        $user->lastip = $request->ip();
        $user->last_login = time();
        $user->laston = time();
        $user->signedup = time();
        $user->will = 100;
        $user->maxwill = 100;
        $user->energy = 12;
        $user->maxenergy = 12;
        $user->brave = 5;
        $user->maxbrave = 5;
        $user->hp = 100;
        $user->maxhp = 100;
        $user->location = 1;
        $user->save();

        $stats = new UserStat;
        $stats->userid = $user->userid;
        $stats->strength = 10;
        $stats->agility = 10;
        $stats->guard = 10;
        $stats->labour = 10;
        $stats->IQ = 10;
        $stats->save();

        $token = JWTAuth::fromUser($user);

        return $this->response->array(['token' => $token]);
    }
}