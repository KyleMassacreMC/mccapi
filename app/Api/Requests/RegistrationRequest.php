<?php
/**
 * Created by PhpStorm.
 * User: kyle
 * Date: 3/27/16
 * Time: 8:23 PM
 */

namespace Api\Requests;


use Dingo\Api\Http\FormRequest;

class RegistrationRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => 'required|max:255|min:5|unique:users,username',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
            'gender' => 'required|in:Male,Female'
        ];
    }
}