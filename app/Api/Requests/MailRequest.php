<?php
/**
 * Created by PhpStorm.
 * User: kyle
 * Date: 3/21/16
 * Time: 4:43 AM
 */

namespace Api\Requests;

use Dingo\Api\Http\FormRequest;

class MailRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mail_to' => 'required|integer|exists:users,userid',
            'mail_subject' => 'min:5',
            'mail_text' => 'required|min:5',
        ];
    }

    public function messages()
    {
        return [
            'mail_to.required' => 'You must supply someone to send the message to',
            'mail_to.integer' => 'The receiver must be a valid userid',
            'mail_to.exists' => 'The receiver does not exist',
            'mail_subject.min' => 'The subject must be at least :min characters',
            'mail.text.required' => 'A message is required',
            'mail.text.min'  => 'The message must be at least :min characters',
        ];
    }
}