<?php

namespace Api\Transformers;

use App\User;
use Carbon\Carbon;
use League\Fractal\TransformerAbstract;

class UserTransformer extends TransformerAbstract
{
    public function transform(User $user)
    {
        return [
            'userid' => (int) $user->userid,
            'username' => $user->username,
            'level' => (int) $user->level,
            'exp' => [
                'acutal' => (float) $user->exp,
                'next_level' => (int) $user->exp_needed,
                'needed' => (int) $user->exp_needed - $user->exp,
                'percent' => (float) $user->exp_needed_percent,
                'to_decimal' => (double) $user->exp_needed_decimal
            ],
            'money' => [
                'actual' => (int) $user->money,
                'formatted' => $user->formatted_money
            ],
            'crystals' => [
                'actual' => (int) $user->crystals,
                'formatted' => $user->formatted_crystals
            ],
            'laston' => [
                'timestamp' => $user->laston,
                'formatted' => Carbon::createFromTimestamp($user->laston),
                'ago' => Carbon::createFromTimestamp($user->laston)->diffForHumans()
            ],
            'lastip' => [
                'ip' => $user->lastip,
                'host' => gethostbyaddr($user->lastip)
            ],
            'job' => [
                'id' => $user->job,
                'job_name' => $user->jobData->jNAME
            ],
            'energy' => [
                'current' => (int) $user->energy,
                'percent' => (float) $user->energy_perc,
                'max' => (int) $user->maxenergy,
                'to_decimal' => (double) $user->energy_percent_decimal
            ],
            'will' => [
                'current' => (int) $user->will,
                'percent' => (float) $user->will_perc,
                'max' => $user->maxwill,
                'to_decimal' => (double) $user->will_percent_decimal
            ],
            'brave' => [
                'current' => (int) $user->brave,
                'percent' => (float) $user->brave_perc,
                'max' => (int) $user->maxbrave,
                'to_decimal' => (double) $user->brave_percent_decimal
            ],
            'hp' => [
                'current' => (int) $user->hp,
                'percent' => (float) $user->hp_perc,
                'max' => (int) $user->maxhp,
                'to_decimal' => (double) $user->hp_percent_decimal
            ],
            'lastrest_life' => '0',
            'lastrest_other' => '0',
            'location' => [
                'location_id' => (int) $user->location,
                'location_name' => $user->city->cityname
            ],
            'hospital' => (int) $user->hospital,
            'jail' => (int) $user->jail,
            'jail_reason' => $user->jail_reason,
            'fedjail' => $user->fedjail,
            'user_level' => $user->user_level,
            'gender' => $user->gender,
            'daysold' => (int) $user->daysold,
            'signedup' => [
                'timestamp' => (int)$user->signedup,
                'formatted' => Carbon::createFromTimestamp($user->signedup)->diffForHumans()
            ],
            'gang' => [
                'gang_id' => (int) $user->gang,
                'gang_name' => !is_null($user->gangData) ? $user->gangData->gangNAME : 'null'
            ],
            'daysingang' => (int) $user->daysingang,
            'course' => [
                'course_id' => (int) $user->course,
                'course_name' => !is_null($user->courseData) ? $user->courseData->crNAME : 'null'
            ],
            'cdays' => (int) $user->cdays,
            // @TODO: Add job ranks
            'jobrank' => '0',
            'donatordays' => (int) $user->donatordays,
            'email' => $user->email,
            'login_name' => $user->login_name,
            'display_pic' => $user->display_pic,
            'duties' => $user->duties,
            'bankmoney' => [
                'acutal' => (int) $user->bankmoney,
                'formatted' => $user->formatted_bank_money
            ],
            'cybermoney' => [
                'acutal' => (int) $user->cybermoney,
                'formatted' => $user->formatted_cyber_money
            ],
            'staffnotes' => $user->staffnotes,
            'mailban' => $user->mailban,
            'mb_reason' => $user->mb_reason,
            'hospreason' => $user->hospreason,
            'lastip_login' => $user->lastip_login,
            'lastip_signup' => $user->lastip_signup,
            'last_login' => [
                'timestamp' => (int)$user->last_login,
                'formatted' => Carbon::createFromTimestamp($user->last_login)->diffForHumans()
            ],
            'voted' => $user->voted,
            'crimexp' => (int) $user->crimexp,
            'attacking' => (int) $user->attacking,
            'verified' => (bool) $user->verified,
            'forumban' => (int)$user->forumban,
            'fb_reason' => $user->fb_reason,
            'posts' => (int) $user->posts,
            'forums_avatar' => $user->forums_avatar,
            'forums_signature' => $user->forums_signature,
            'new_events' => (int) $user->new_events,
            'new_mail' => (int) $user->new_mail,
            'friend_count' => (int) $user->friend_count,
            'enemy_count' => (int) $user->enemy_count,
            'new_announcements' => (int) $user->new_announcements,
            'boxes_opened' => (int) $user->boxes_opened,
            'user_notepad' => $user->user_notepad,
            'equip_primary' => [
                'item_id' => (int) $user->equip_primary,
                'item_name' => !is_null($user->primaryWeapon) ? $user->primaryWeapon->itmname : 'null',
                'power' => !is_null($user->primaryWeapon) ? (int) $user->primaryWeapon->weapon : 'null',
                'item_type' => !is_null($user->primaryWeapon) ? $user->primaryWeapon->itemType->itmtypename : 'null'
            ],
            'equip_secondary' => [
                'item_id' => (int) $user->equip_secondary,
                'item_name' => !is_null($user->secondaryWeapon) ? $user->secondaryWeapon->itmname : 'null',
                'power' => !is_null($user->secondaryWeapon) ? (int) $user->secondaryWeapon->weapon : 'null',
                'item_type' => !is_null($user->secondaryWeapon) ? $user->secondaryWeapon->itemType->itmtypename : 'null'
            ],
            'equip_armor' => [
                'item_id' => (int) $user->equip_armor,
                'item_name' => !is_null($user->equippedArmor) ? $user->equippedArmor->itmname : 'null',
                'guard' => !is_null($user->equippedArmor) ? (int) $user->equippedArmor->armor : 'null',
                'item_type' => !is_null($user->equippedArmor) ? $user->equippedArmor->itemType->itmtypename : 'null'
            ],
            'force_logout' => (bool) $user->force_logout,
            'user_stats' => [
                'Strength' => [
                    'actual' => (float) $user->stats->strength,
                    'formatted' => $user->stats->formatted_strength
                ],
                'Guard' => [
                    'actual' => (float) $user->stats->guard,
                    'formatted' => $user->stats->formatted_guard
                ],
                'Labour' => [
                    'actual' => (float) $user->stats->labour,
                    'formatted' => $user->stats->formatted_labour
                ],
                'Agility' => [
                    'acutal' => (float) $user->stats->agility,
                    'formatted' => $user->stats->formatted_agility
                ],
                'IQ' => [
                    'actual' => (float) $user->stats->IQ,
                    'formatted' => $user->stats->formatted_iq
                ]
            ]
        ];
        
    }
}