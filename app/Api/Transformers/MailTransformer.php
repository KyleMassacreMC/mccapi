<?php
namespace Api\Transformers;

use App\Messages;
use League\Fractal\TransformerAbstract;

class MailTransformer extends TransformerAbstract
{
    public function transform(Messages $messages)
    {
        return [
            'message_id' => (int)$messages->mail_id,
            'mail_subject' => $messages->mail_subject,
            'mail_text' => $messages->mail_text,
            'message_read' => (bool)$messages->mail_read,
            'mail_from' => [
                'userid' => (int)$messages->mail_from,
                'username' => $messages->sender->username,
            ],
            'mail_to' => [
              'userid' => (int)$messages->mail_to,
              'username' => $messages->receiver->username,
            ],
            'times' => [
                'timestamp' => $messages->mail_time,
                'formatted' => \Carbon\Carbon::createFromTimestamp($messages->mail_time)
            ],
            'read_link' => route('api.messages.show',[$messages->mail_id]),
            'delete_link' => route('api.messages.delete',[$messages->mail_id])
        ];
    }
}
