<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ItemType extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'itemtypes';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    protected $primaryKey = 'itmtypeid';

    public $timestamps = false;

    public function items()
    {
        return $this->hasMany('App\Item','itmtype','itmtypeid');
    }
}
