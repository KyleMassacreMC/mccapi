<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gang extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'gangs';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['*'];

    protected $primaryKey = 'gangID';

    public $timestamps = false;

    public function userData()
    {
        return $this->hasMany('App\User','gang');
    }
}
