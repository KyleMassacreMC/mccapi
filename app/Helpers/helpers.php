<?php

/**
 * @param string $input The direct input of the users password
 * @param string $salt  The users salt to be compared to with the password
 * @param string $pass  The users password from the database
 *
 * @return bool         The result of the password matching or not
 */
function verify_user_password($input, $salt, $pass)
{
    return ($pass === encode_password($input, $salt));
}

/**
 * Given a password and a salt, encode them to the form which is stored in
 * the game's database.
 *
 * @param string  $password       The password to be encoded
 * @param string  $salt           The user's unique pass salt
 * @param boolean $already_md5    Whether the specified password is already
 *                                a md5 hash. This would be true for legacy
 *                                v2 passwords.
 *
 * @return string    The resulting encoded password.
 */
function encode_password($password, $salt, $already_md5 = false)
{
    if (!$already_md5)
    {
        $password = call_user_func(env('HASH_TYPE'),$password);
    }
    return call_user_func(env('HASH_TYPE'),$salt . $password);
}

/**
 * Generate a salt to use to secure a user's password
 * from rainbow table attacks.
 *
 * @return string    The generated salt, 8 alphanumeric characters
 */
function generate_pass_salt()
{
    return substr(call_user_func(env('HASH_TYPE'),microtime(true)), 0, 8);
}

/**
 * @param mixed $value The value of what you would like to format for currency
 *
 * @return string           A number formatted value with a currency symbol
 */
function money_formatter($value)
{
    return env('CURRENCY_SYMBOL', '$') . number_format($value);
}