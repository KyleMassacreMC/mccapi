<?php
$api = app('Dingo\Api\Routing\Router');

// Version 1 of our API
$api->version('v1', function ($api) {

	// Set our namespace for the underlying routes
	$api->group(['namespace' => 'Api\Controllers', 'middleware' => 'cors'], function ($api) {

		// Login route
		$api->post('login', ['as' => 'login', 'uses' => 'AuthController@authenticate']);
		$api->post('register', ['as' => 'register', 'uses' => 'AuthController@register']);
        $api->post('test', 'AuthController@me');
		$api->group( [ 'middleware' => 'jwt.auth' ], function ($api) {

			/*
			 * Message Routes
			 */
			$api->match(['get', 'head'],'/messages',['as' => 'api.messages.index', 'uses' => 'MessagesController@index']);
			$api->post('messages',['as' => 'api.messages.store', 'uses' => 'MessagesController@store']);
			$api->match(['get','head'],'messages/{messages}',['as' => 'api.messages.show', 'uses' => 'MessagesController@show']);
			$api->delete('messages/{messages}',['as' => 'api.messages.delete', 'uses' => 'MessagesController@destroy']);


			$api->get('users/me', ['as' => 'users.me', 'uses' => 'UsersController@me']);
			$api->get('users/validate_token', ['as' => 'users.validate', 'uses' => 'AuthController@validateToken']);
            $api->get('users/logout', ['as' => 'users.logout', 'uses' => 'UsersController@logout']);

		});

	});

});