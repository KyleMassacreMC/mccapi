<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract,
                                    JWTSubject
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['userpass', 'pass_salt'];

    protected $primaryKey = 'userid';

    public $timestamps = false;

    /**
     * Get the identifier that will be stored in the subject claim of the JWT
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->attributes[$this->primaryKey];
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /*
     * Relations
     */

    public function stats()
    {
        return $this->hasOne('App\UserStat', 'userid', 'userid');
    }

    public function jobData()
    {
        return $this->hasOne('App\Job', 'jID', 'job');
    }
    
    public function city()
    {
        return $this->hasOne('App\City', 'cityid', 'location');
    }

    public function gangData()
    {
        return $this->hasOne('App\Gang', 'gangID', 'gang');
    }

    public function courseData()
    {
        return $this->hasOne('App\Course', 'crID', 'course');
    }

    public function primaryWeapon()
    {
        return $this->hasOne('App\Item', 'itmid', 'equip_primary');
    }

    public function secondaryWeapon()
    {
        return $this->hasOne('App\Item', 'itmid', 'equip_secondary');
    }

    public function equippedArmor()
    {
        return $this->hasOne('App\Item', 'itmid', 'equip_armor');
    }

    /*
     * Some Extras
     */

    public function decrementNewMail()
    {
        if ($this->attributes['new_mail'] > 0)
        {
            return $this->decrement('new_mail');
        }
    }

    public function incrementNewMail()
    {
        return $this->increment('new_mail');
    }
    
    function check_level()
    {
        $xpNeeded = $this->getExpNeededAttribute();

        if($this->attributes['exp'] > $xpNeeded) {
            $expu = $this->attributes['exp'] - $xpNeeded;
            $this->attributes['level'] += 1;
            $this->attributes['exp'] = $expu;
            $this->attributes['energy'] += 2;
            $this->attributes['brave'] += 2;
            $this->attributes['maxenergy'] += 2;
            $this->attributes['maxbrave'] += 2;
            $this->attributes['hp'] += 50;
            $this->attributes['maxhp'] += 50;
            $this->save();

        }
    }

    /*
     * Mutators
     */

    public function getFormattedMoneyAttribute()
    {
        return money_formatter($this->attributes['money']);
    }

    public function getFormattedCrystalsAttribute()
    {
        return number_format($this->attributes['crystals']);
    }

    public function getFormattedBankMoneyAttribute()
    {
        return money_formatter($this->attributes['bankmoney']);
    }

    public function getFormattedCyberMoneyAttribute()
    {
        return number_format($this->attributes['cybermoney']);
    }

    public function getEnergyPercAttribute()
    {
        return round($this->attributes['energy'] / $this->attributes['maxenergy'] * 100, 1);
    }

    public function getEnergyPercentDecimalAttribute()
    {
        return round($this->attributes['energy'] / $this->attributes['maxenergy'], 3);
    }

    public function getWillPercAttribute()
    {
        return round($this->attributes['will'] / $this->attributes['maxwill'] * 100, 1);
    }

    public function getWillPercentDecimalAttribute()
    {
        return round($this->attributes['will'] / $this->attributes['maxwill'] , 3);
    }

    public function getBravePercAttribute()
    {
        return round($this->attributes['brave'] / $this->attributes['maxbrave'] * 100, 1);
    }

    public function getBravePercentDecimalAttribute()
    {
        return round($this->attributes['brave'] / $this->attributes['maxbrave'] , 3);
    }

    public function getHpPercAttribute()
    {
        return round($this->attributes['hp'] / $this->attributes['maxhp'] * 100, 1);
    }

    public function getHpPercentDecimalAttribute()
    {
        return round($this->attributes['hp'] / $this->attributes['maxhp'], 3);
    }

    public function getExpNeededAttribute()
    {
        return (int)(($this->attributes['level'] + 1) * ($this->attributes['level'] + 1)
            * ($this->attributes['level'] + 1) * 2.2);
    }

    public function getExpNeededPercentAttribute()
    {
        return round($this->attributes['exp'] / $this->getExpNeededAttribute() * 100, 1);
    }

    public function getExpNeededDecimalAttribute()
    {
        return round($this->attributes['exp'] / $this->getExpNeededAttribute(),3);
    }

}
