<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Messages extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'mail_id';

    protected $table = 'mail';

    protected $fillable = ['*'];

    public function sender()
    {
        return $this->belongsTo('App\User','mail_from','userid');
    }

    public function receiver()
    {
        return $this->belongsTo('App\User','mail_to','userid');
    }
}
