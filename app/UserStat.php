<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserStat extends Model
{
    public $timestamps = false;

    protected $primaryKey = 'userid';

    protected $table = 'userstats';

    protected $fillable = ['*'];
    
    public function getFormattedStrengthAttribute()
    {
        return number_format($this->attributes['strength']);
    }

    public function getFormattedAgilityAttribute()
    {
        return number_format($this->attributes['agility']);
    }

    public function getFormattedGuardAttribute()
    {
        return number_format($this->attributes['guard']);
    }

    public function getFormattedLabourAttribute()
    {
        return number_format($this->attributes['labour']);
    }

    public function getFormattedIqAttribute()
    {
        return number_format($this->attributes['IQ']);
    }
}
