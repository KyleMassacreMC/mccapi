## vue-starter Backend API (Laravel-based)

This application will serve as the companion app to another project called vue-starter. It is meant to be a small demo of a Laravel API, using Dingo and JWT for authentication.

[vue-starter Frontend App](https://github.com/layer7be/vue-starter)

## Installation

### Step 1: Clone the repo
```
git clone git@bitbucket.org:KyleMassacreMC/mccapi.git
```
Or
```
git clone https://KyleMassacreMC@bitbucket.org/KyleMassacreMC/mccapi.git
```

### Step 2: Prerequisites

#### **Note:** **_Before following the next step, please ensure that you have a .env file. If not, copy .env.example and save it as .env_**
```
composer install
php artisan key:generate
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"
php artisan jwt:secret
```
### Step 3: Setup environment
Open your .env file that was either made for you or that you copied from your .env.example and fill out the following keys:
```
APP_ENV=local
APP_DEBUG=true
DB_CONNECTION=mysql
DB_HOST=localhost
DB_DATABASE=homestead
DB_USERNAME=homestead
DB_PASSWORD=secret
API_NAME=VendorName
API_DEBUG=true
API_SUBTYPE=myapp
HASH_TYPE=md5
CURRENCY_SYMBOL="$"
```

### Step 4: Serve
```
php artisan serve
```

### Note about Apache
If you use Apache to serve this, you will need to add the following 2 lines to your .htaccess (or your virtualhost configuration):
```
RewriteCond %{HTTP:Authorization} ^(.*)
RewriteRule .* - [e=HTTP_AUTHORIZATION:%1]
```
## Current Routes

| Host | URI                                | Name                 |
|------|:-----------------------------------|---------------------:|
|      | POST /api/login                    | login                |
|      | POST /api/register                 | register             |
|      | GET|HEAD /api/messages             | api.messages.index   |
|      | POST /api/messages                 | api.messages.store   |
|      | GET|HEAD /api/messages/{messages}  | api.messages.show    |
|      | DELETE /api/messages/{messages}    | api.messages.destroy |
|      | GET|HEAD /api/users/me             | users.me             |
|      | GET|HEAD /api/users/validate_token | users.validate       |


## Sample Response for the Users
### Route: /api/users/me
```JSON
{
  "data": {
    "userid": 1,
    "username": "KyleMassacre",
    "level": 1,
    "exp": 10,
    "money": {
      "actual": 99995,
      "formatted": "$99,995"
    },
    "crystals": {
      "actual": 100000,
      "formatted": "100,000"
    },
    "laston": {
      "timestamp": "1458905074",
      "formatted": {
        "date": "2016-03-25 11:24:34.000000",
        "timezone_type": 3,
        "timezone": "UTC"
      },
      "ago": "33 minutes ago"
    },
    "lastip": {
      "ip": "127.0.0.1",
      "host": "localhost"
    },
    "job": {
      "id": "1",
      "job_name": "Badass"
    },
    "energy": {
      "current": 8,
      "percent": 66.7,
      "max": 12
    },
    "will": {
      "current": 28,
      "percent": 28,
      "max": "100"
    },
    "brave": {
      "current": 2,
      "percent": 40,
      "max": 5
    },
    "hp": {
      "current": 39,
      "percent": 39,
      "max": 100
    },
    "lastrest_life": "0",
    "lastrest_other": "0",
    "location": {
      "location_id": 1,
      "location_name": "Default City"
    },
    "hospital": 0,
    "jail": 0,
    "jail_reason": "",
    "fedjail": "0",
    "user_level": "2",
    "gender": "Male",
    "daysold": 0,
    "signedup": {
      "timestamp": 1420385317,
      "formatted": "1 year ago"
    },
    "gang": {
      "gang_id": 1,
      "gang_name": "Test Gang"
    },
    "daysingang": 0,
    "course": {
      "course_id": 0,
      "course_name": "null"
    },
    "cdays": 0,
    "jobrank": "0",
    "donatordays": 0,
    "email": "ky.ellis83@gmail.com",
    "login_name": "KyleMassacre",
    "display_pic": "http://cdn.smosh.com/sites/default/files/ftpuploads/bloguploads/0913/harry-potter-memes-potter-lookin-fine.jpg",
    "duties": "N/A",
    "bankmoney": {
      "acutal": 0,
      "formatted": "$0"
    },
    "cybermoney": {
      "acutal": -1,
      "formatted": "-1"
    },
    "staffnotes": "",
    "mailban": "0",
    "mb_reason": "",
    "hospreason": "",
    "lastip_login": "127.0.0.1",
    "lastip_signup": "127.0.0.1",
    "last_login": {
      "timestamp": 1458905074,
      "formatted": "33 minutes ago"
    },
    "voted": "",
    "crimexp": 0,
    "attacking": 0,
    "verified": false,
    "forumban": 0,
    "fb_reason": "",
    "posts": 0,
    "forums_avatar": "",
    "forums_signature": "",
    "new_events": 0,
    "new_mail": 0,
    "friend_count": 0,
    "enemy_count": 0,
    "new_announcements": 0,
    "boxes_opened": 0,
    "user_notepad": "",
    "equip_primary": {
      "item_id": 1,
      "item_name": "AK-47",
      "power": 10,
      "item_type": "Weapon"
    },
    "equip_secondary": {
      "item_id": 2,
      "item_name": "Glock",
      "power": 5,
      "item_type": "Handheld"
    },
    "equip_armor": {
      "item_id": 3,
      "item_name": "Bullet-Proof Vest",
      "guard": 10,
      "item_type": "Armor"
    },
    "force_logout": false,
    "user_stats": {
      "Strength": {
        "actual": 10,
        "formatted": "10"
      },
      "Guard": {
        "actual": 10,
        "formatted": "10"
      },
      "Labour": {
        "actual": 10,
        "formatted": "10"
      },
      "Agility": {
        "acutal": 10,
        "formatted": "10"
      },
      "IQ": {
        "actual": 10,
        "formatted": "10"
      }
    }
  }
}
```


